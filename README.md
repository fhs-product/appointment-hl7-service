<!-- ABOUT THE PROJECT -->
## About The Project

This is a sample fhir implimentation for booking a new appointment.

### Built With

* [Java 8](https://www.oracle.com/java/technologies/java8.html)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [Maven 3](https://maven.apache.org/index.html)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites
Make sure you have docker installed on your machine
  ```sh
  docker --version
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/viettrung/hl7-fhir-demo.git
   ```
2. Open terminal and navigate the the root folder of the cloned project
   ```sh
   cd <path-to>/hl7-fhir-demo
   ```
3. Run docker command
   ```
   docker-compose up
   ```
   
> *Note:* To make **Keycloak** work, you need to add the following line to your hosts file (**/etc/hosts** on Mac/Linux, **c:\Windows\System32\Drivers\etc\hosts** on Windows).
   ```sh
   127.0.0.1	keycloak
   ```

   
<!-- USAGE EXAMPLES -->
## Usage

### Examples

1. Create a new user
   ```sh
   curl -X POST "http://localhost:9092/users" \
      -H "accept: */*" \
      -H "Content-Type: application/json" \
      -d '{"username": "test", "firstName": "first Name", 	"lastName": "last Name", "password": "test" }'
   ```

2. Get all users
   ```sh
   curl -X GET "http://localhost:9092/users"
   ```

3. Get access token
   ```sh
   curl -X POST 'http://keycloak:8080/auth/realms/fhir/protocol/openid-connect/token' \
      --header 'Content-Type: application/x-www-form-urlencoded' \
      --data-urlencode 'grant_type=password' \
      --data-urlencode 'client_id=fhir-service' \
      --data-urlencode 'username=test' \
      --data-urlencode 'password=test'
   ```

4. Get all appointments
   ```sh
   curl -X GET "http://localhost:9091/Appointment" \
      -H "accept: application/json" \
      -H "Content-Type: application/json" \
      -H "Authorization: bearer <ACCESS_TOKEN>"
   ```

5. Create new appointment
   ```sh
   curl -X POST "http://localhost:9091/Appointment" \
      -H "accept: application/json" \
      -H "Content-Type: application/json" \
      -H "Authorization: bearer <ACCESS_TOKEN>" \
      -d "<APPOINTMENT_JSON_DATA>"
   ```

6. Update an existing appointment
   ```sh
   curl -X PUT "http://localhost:9091/Appointment/{id}" \
      -H "accept: application/json" \
      -H "Content-Type: application/json" \
      -H "Authorization: bearer <ACCESS_TOKEN>" \
      -d "<APPOINTMENT_JSON_DATA>"
   ```

7. Find appointment by its version
   ```sh
   curl -X GET "http://localhost:9091/Appointment/{id}/_history/{versionId}" \
      -H "accept: application/json" \
      -H "Content-Type: application/json" \
      -H "Authorization: bearer <ACCESS_TOKEN>"
   ```


---

### References

* [HL7 FHIR - Release 4](https://www.hl7.org/fhir/)
* [Should you use FHIR resources as your storage format?](https://medium.com/fhirbase-dojo/should-you-use-fhir-resources-as-your-storage-format-62dd25977a85)
* [fhirbase](https://fhirbase.aidbox.app/schema)
   * [Demo](https://fbdemo.aidbox.app/)
* [Using JSONB in PostgreSQL](https://scalegrid.io/blog/using-jsonb-in-postgresql-how-to-effectively-store-index-json-data-in-postgresql/)
   * [JSONB Operators & Functions](https://www.postgresql.org/docs/12/functions-json.html#FUNCTIONS-JSON-PROCESSING)
   [How to Query a Postgres JSONB Column](https://kb.objectrocket.com/postgresql/how-to-query-a-postgres-jsonb-column-1433)
   * [Spring Boot with PostgreSQL, Flyway, and JSONB](https://developer.okta.com/blog/2019/02/20/spring-boot-with-postgresql-flyway-jsonb)