package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.Appointment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;

import com.fhir.example.domain.resource.AppointmentRs;

@Mapper(
		componentModel = "spring",
		nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
		nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
		uses = {
				NarrativeMapper.class,
				MetaMapper.class,
				IdentifierMapper.class,
				CodeableConceptMapper.class,
				ReferenceMapper.class,
				ParticipantMapper.class,
				PeriodMapper.class
		}
		)
public interface AppointmentMapper extends BaseMapper {

    @Mapping(source = "model", target = "id", qualifiedByName = "idTypeToUuid")
	AppointmentRs toResource(Appointment model);

	@Mapping(source = "id", target = "id", qualifiedByName = "uuidToString")
	Appointment toModel(AppointmentRs resource);

	List<Appointment> toModels(List<AppointmentRs> resources);
	
}
