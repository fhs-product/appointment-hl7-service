package com.fhir.example.mapper;

import org.hl7.fhir.r4.model.Meta;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueMappingStrategy;

import com.fhir.example.domain.resource.MetaRs;

@Mapper(
	componentModel = "spring", 
	nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT,
	uses = { CodingMapper.class }
)
public interface MetaMapper {
	@Mapping(target = "lastUpdated", ignore = true)
	MetaRs toResource(Meta model);

	@Mapping(source = "version", target = "versionId")
	Meta toModel(MetaRs resource);
}
