package com.fhir.example.resource.provider;

import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Enumerations.ResourceType;
import org.hl7.fhir.r4.model.Slot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fhir.example.domain.resource.SlotRs;
import com.fhir.example.mapper.SlotMapper;
import com.fhir.example.service.SlotService;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;

@Component
public class SlotResourceProvider implements IResourceProvider {

	@Autowired
	private SlotService slotService;

	@Autowired
	private SlotMapper slotMapper;

	@Override
	public Class<? extends IBaseResource> getResourceType() {
		return Slot.class;
	}

	@Create
	public MethodOutcome create(@ResourceParam Slot slot) throws Exception {

		SlotRs slotRs = slotService.create(slotMapper.toResource(slot));

		// Build response containing the new resource id
		MethodOutcome methodOutcome = new MethodOutcome();
		methodOutcome.setId(new IdDt(ResourceType.SLOT.getDisplay(), slotRs.getId().toString()));
		methodOutcome.setResource(slotMapper.toModel(slotRs));
		methodOutcome.setCreated(Boolean.TRUE);

		return methodOutcome;
	}
}