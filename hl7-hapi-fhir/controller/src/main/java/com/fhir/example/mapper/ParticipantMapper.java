package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.Appointment.AppointmentParticipantComponent;
import org.mapstruct.Mapper;

import com.fhir.example.domain.resource.ParticipantRs;

@Mapper(
	componentModel = "spring", 
	uses = {
		CodeableConceptMapper.class, 
		ReferenceMapper.class,
		PeriodMapper.class
	}
)
public interface ParticipantMapper {
	ParticipantRs toResource(AppointmentParticipantComponent model);
	AppointmentParticipantComponent toModel(ParticipantRs resource);

	List<ParticipantRs> toResources(List<AppointmentParticipantComponent> models);
	List<AppointmentParticipantComponent> toModels(List<AppointmentParticipantComponent> resources);
}
