package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.CodeableConcept;
import org.mapstruct.Mapper;

import com.fhir.example.domain.resource.CodeableConceptRs;

@Mapper(componentModel = "spring", uses = {CodingMapper.class})
public interface CodeableConceptMapper {
	CodeableConceptRs toResource(CodeableConcept model);
	CodeableConcept toModel(CodeableConceptRs resource);

	List<CodeableConceptRs> toResources(List<CodeableConcept> models);
	List<CodeableConcept> toModels(List<CodeableConceptRs> resources);
}
