package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.Period;
import org.mapstruct.Mapper;

import com.fhir.example.domain.resource.PeriodRs;

@Mapper(componentModel = "spring")
public interface PeriodMapper {
	PeriodRs toResource(Period model);
	Period toModel(PeriodRs resource);

	List<PeriodRs> toResources(List<Period> model);
	List<Period> toModels(List<PeriodRs> resources);
}
