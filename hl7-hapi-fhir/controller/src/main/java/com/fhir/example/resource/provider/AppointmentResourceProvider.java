package com.fhir.example.resource.provider;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Appointment;
import org.hl7.fhir.r4.model.Enumerations.ResourceType;
import org.hl7.fhir.r4.model.IdType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fhir.example.domain.resource.AppointmentRs;
import com.fhir.example.mapper.AppointmentMapper;
import com.fhir.example.service.AppointmentService;
import com.fhir.example.service.SlotService;

import ca.uhn.fhir.model.primitive.IdDt;
import ca.uhn.fhir.rest.annotation.Create;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.ResourceParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.annotation.Update;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;

@Component
public class AppointmentResourceProvider implements IResourceProvider {

	private final Logger LOGGER = LoggerFactory.getLogger(AppointmentResourceProvider.class);

	@Autowired
	private AppointmentService appointmentService;

	@Autowired
	private SlotService slotService;

	@Autowired
	private AppointmentMapper appointmentMapper;

	@Override
	public Class<? extends IBaseResource> getResourceType() {
		return Appointment.class;
	}

	@Search()
	public List<Appointment> searchAllAppointments() {
		List<AppointmentRs> appointmentRs = appointmentService.findAll();

		return appointmentMapper.toModels(appointmentRs);
	}

	@Search
	public List<Appointment> getAppointmentsByParticipant(@RequiredParam(name = "actorName") final String actorName) {
		List<AppointmentRs> appointments = appointmentService.findByActorDisplayName(actorName);

		return appointmentMapper.toModels(appointments);
	}

	@Read(version=true)
	public Appointment read(@IdParam final IdType theId) {
		AppointmentRs appointmentRs = null;

		UUID id = UUID.fromString(theId.getIdPart());

		if (theId.hasVersionIdPart()) {
			appointmentRs = appointmentService.findByHistory(id, theId.getVersionIdPartAsLong());
		} else {
			appointmentRs = appointmentService.findById(id);
		}

		return appointmentMapper.toModel(appointmentRs);
	}

	@Create
	public MethodOutcome createAppointment(@ResourceParam final Appointment appointment) throws Exception {

		// Slots must existed
		//		for (Reference slot : appointment.getSlot()) {
		//			if (ResourceType.SLOT.getDisplay().equals(slot.getReferenceElement().getResourceType())) {
		//				String slotId = slot.getReferenceElement().getIdPart();
		//				SlotRs slotRs = slotService.findById(UUID.fromString(slotId));
		//				if (slotRs == null) {
		//					throw new UnprocessableEntityException(String.format("Slot resource reference value %s is not a valid resource.", slotId));
		//				}
		//			}
		//		}

		// convert dto and call service to create appointment
		AppointmentRs appointmentRs = appointmentService.create(appointmentMapper.toResource(appointment));

		// Build response containing the new resource id
		MethodOutcome methodOutcome = new MethodOutcome();
		methodOutcome.setId(new IdDt(ResourceType.APPOINTMENT.getDisplay(), appointmentRs.getId().toString()));
		methodOutcome.setResource(appointmentMapper.toModel(appointmentRs));
		methodOutcome.setCreated(Boolean.TRUE);

		return methodOutcome;
	}

	@Update
	public MethodOutcome updateAppointment(@IdParam final IdType appointmentId, @ResourceParam final Appointment appointment, final HttpServletRequest theRequest) {

		UUID id = UUID.fromString(appointmentId.getIdPart());
		AppointmentRs appointmentRs = appointmentService.update(id, appointmentMapper.toResource(appointment));

		MethodOutcome methodOutcome = new MethodOutcome();
		methodOutcome.setId(new IdDt(ResourceType.APPOINTMENT.getDisplay(), appointmentRs.getId().toString()));
		methodOutcome.setResource(appointmentMapper.toModel(appointmentRs));
		methodOutcome.setCreated(Boolean.TRUE);

		return methodOutcome;
	}
}