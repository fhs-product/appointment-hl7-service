package com.fhir.example.mapper;

import org.hl7.fhir.r4.model.Narrative;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.fhir.example.domain.resource.NarrativeRs;

@Mapper(componentModel = "spring")
public interface NarrativeMapper {
	
	@Mapping(target = "div", ignore = true)
	NarrativeRs toResource(Narrative model);

	@Mapping(target = "div", ignore = true)
	Narrative toModel(NarrativeRs resource);
}
