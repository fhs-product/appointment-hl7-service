package com.fhir.example.mapper;

import java.util.UUID;

import org.hl7.fhir.r4.model.Appointment;
import org.mapstruct.Named;

public interface BaseMapper {
	
	@Named("idTypeToUuid") 
    public static UUID idTypeToUuid(Appointment model) {
		if (model.hasId()) {
			return UUID.fromString(model.getIdElement().getIdPart()); 
		}
		return UUID.randomUUID();
    }

	@Named("uuidToString") 
	public static String uuidToString(UUID id) { 
		return id.toString();
	}
}
