package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.Reference;
import org.mapstruct.Mapper;

import com.fhir.example.domain.resource.ReferenceRs;

@Mapper(
	componentModel = "spring", 
	uses = {
		IdentifierMapper.class
	}
)
public interface ReferenceMapper {
	ReferenceRs toResource(Reference model);
	Reference toModel(ReferenceRs resource);
	
	List<ReferenceRs> toResources(List<Reference> models);
	List<Reference> toModels(List<ReferenceRs> resources);
}
