package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.Identifier;
import org.mapstruct.Mapper;

import com.fhir.example.domain.resource.IdentifierRs;

@Mapper(
	componentModel = "spring", 
	uses = {
		CodeableConceptMapper.class, 
		PeriodMapper.class
	}
)
public interface IdentifierMapper {
	IdentifierRs toResource(Identifier model);
	Identifier toModel(IdentifierRs resource);

	List<IdentifierRs> toResources(List<Identifier> dtos);
	List<Identifier> toModels(List<IdentifierRs> resources);
}
