package com.fhir.example.mapper;

import java.util.List;

import org.hl7.fhir.r4.model.Coding;
import org.mapstruct.Mapper;

import com.fhir.example.domain.resource.CodingRs;

@Mapper(componentModel = "spring")
public interface CodingMapper {
	CodingRs toResource(Coding model);
	Coding toModel(CodingRs resource);

	List<CodingRs> toResources(List<Coding> models);
	List<Coding> toModels(List<CodingRs> resources);
}
