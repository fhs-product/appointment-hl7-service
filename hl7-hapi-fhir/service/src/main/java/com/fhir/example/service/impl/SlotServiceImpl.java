package com.fhir.example.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fhir.example.domain.SlotEntity;
import com.fhir.example.domain.resource.SlotRs;
import com.fhir.example.repository.SlotRepository;
import com.fhir.example.service.SlotService;

@Service("slotService")
public class SlotServiceImpl implements SlotService {

	@Autowired
	private SlotRepository slotRepository;

	@Override
	public SlotRs create(final SlotRs slot) {

		SlotEntity entity = SlotEntity.builder()
				.id(slot.getId())
				.createdBy(null)
				.modifiedBy(null)
				.resource(slot).build();

		entity = slotRepository.save(entity);

		return entity.getResource();
	}

	@Override
	public SlotRs findById(final UUID id) {
		SlotEntity entity = slotRepository.findById(id).orElse(null);
		return entity == null ? null : entity.getResource();
	}

}
