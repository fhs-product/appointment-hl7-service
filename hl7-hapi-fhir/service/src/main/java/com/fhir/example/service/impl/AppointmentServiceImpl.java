package com.fhir.example.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.persistence.OptimisticLockException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhir.example.domain.AppointmentEntity;
import com.fhir.example.domain.AppointmentHistoryEntity;
import com.fhir.example.domain.HistoryEntityId;
import com.fhir.example.domain.resource.AppointmentRs;
import com.fhir.example.domain.resource.MetaRs;
import com.fhir.example.domain.resource.ReferenceRs;
import com.fhir.example.repository.AppointmentHistoryRepository;
import com.fhir.example.repository.AppointmentRepository;
import com.fhir.example.repository.SlotRepository;
import com.fhir.example.service.AppointmentService;

@Service("appointmentService")
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private SlotRepository slotRepository;

	@Autowired
	private AppointmentHistoryRepository appointmentHistoryRepository;

	@Override
	public AppointmentRs create(final AppointmentRs appointment) {

		// slot must exist
		for (ReferenceRs slotReference : appointment.getSlot()) {
			String slotId = slotReference.getReference().split("/")[1];
			slotRepository.findById(UUID.fromString(slotId)).orElseThrow();
		}
		AppointmentEntity entity = AppointmentEntity.builder()
				.id(appointment.getId())
				.resource(appointment)
				.createdBy(null)
				.modifiedBy(null)
				.build();

		entity = appointmentRepository.save(entity);

		return entity.getResource();
	}

	@Override
	public AppointmentRs findById(final UUID id) {
		AppointmentEntity entity = appointmentRepository.findById(id).orElseThrow();
		return entity.getResource();
	}

	@Override
	public List<AppointmentRs> findAll() {

		Iterable<AppointmentEntity> entities = appointmentRepository.findAll();

		List<AppointmentRs> resources = StreamSupport.stream(entities.spliterator(), false)
				.map(AppointmentEntity::getResource).collect(Collectors.toList());

		return resources;
	}

	/**
	 update appointment
set resource = jsonb_set(
					jsonb_set(jsonb_set(resource, '{meta,tag}', '"test 1"',false ), '{priority}', '5',false ),
					'{meta, versionId}',
					(COALESCE(resource->'meta'->'versionId')::int + 1)::text::jsonb,
					false )
WHERE id = '8fe0adaf-d253-476e-92ca-a375f1f12692';
	 */
	@Transactional
	@Override
	public AppointmentRs update(final UUID id, final AppointmentRs appointmentRs) {
		AppointmentEntity existingEntity = appointmentRepository.findById(id).orElse(null);
		if (existingEntity == null) {
			return create(appointmentRs);
		}

		AppointmentRs existingAppointment = existingEntity.getResource();

		// tracking history
		appointmentHistoryRepository.save(AppointmentHistoryEntity.builder()
				.id(HistoryEntityId.builder()
						.id(existingEntity.getId())
						.version(existingEntity.getVersion())
						.build())
				.resource(existingAppointment)
				.build());

		// update existing resource
		MetaRs metaRs = existingAppointment.getMeta();
		Long currentVersion = metaRs.getVersion();
		metaRs = existingAppointment.getMeta().toBuilder().version(metaRs.getVersion() + 1).lastUpdated(LocalDateTime.now()).build();

		AppointmentRs newAppointmentRs = appointmentRs.toBuilder().meta(metaRs).build();

		int result = appointmentRepository.updateResource(id, currentVersion, newAppointmentRs);
		if (result != 1) {
			throw new OptimisticLockException("OptimisticLockException...");
		}
		System.out.println("updated result: "+result);

		return newAppointmentRs;
	}

	@Override
	public AppointmentRs findByHistory(final UUID id, final Long versionId) {
		AppointmentHistoryEntity appointmentHistoryEntity = appointmentHistoryRepository.findById(HistoryEntityId.builder().id(id).version(versionId).build()).orElseThrow();

		return appointmentHistoryEntity.getResource();
	}

	// https://paquier.xyz/postgresql-2/postgres-12-jsonpath/
	@Override
	public List<AppointmentRs> findByActorDisplayName(final String actorDisplayName) {

		List<AppointmentEntity> entities = appointmentRepository.findByActorDisplayName(actorDisplayName);

		return StreamSupport.stream(entities.spliterator(), false)
				.map(AppointmentEntity::getResource).collect(Collectors.toList());
	}

}
