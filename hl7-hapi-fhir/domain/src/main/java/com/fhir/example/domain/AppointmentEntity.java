package com.fhir.example.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Type;

import com.fhir.example.domain.resource.AppointmentRs;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "appointment")
public class AppointmentEntity extends BaseEntity {

	@Id @Type(type = "pg-uuid")
	private UUID id;

	@Builder.Default
	private LocalDateTime createdDate = LocalDateTime.now();

	@Builder.Default
	private LocalDateTime modifiedDate = LocalDateTime.now();

	private String createdBy;

	private String modifiedBy;

	@Version
	@Builder.Default
	private Long version = 1L;

	@Type(type = "jsonb")
	@Column(columnDefinition = "jsonb")
	private AppointmentRs resource;

	//	public static class AppointmentEntityBuilder {
	//		private AppointmentRs resource;
	//
	//		// custom setter for resource: primary key and resource.id should be the same
	//		public AppointmentEntityBuilder resource(AppointmentRs resource) {
	//			if (this.id == null) { // in case of updating, no need to set the id
	//				this.id(resource.getId());
	//			}
	//			this.resource = resource;
	//			return this;
	//		}
	//	}
}
