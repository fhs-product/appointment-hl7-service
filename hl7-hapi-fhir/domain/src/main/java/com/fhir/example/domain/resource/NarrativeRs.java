package com.fhir.example.domain.resource;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class NarrativeRs implements Serializable {
	protected String status;
	protected String div;
}
