package com.fhir.example.domain;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Type;

import com.fhir.example.domain.resource.SlotRs;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "slot")
public class SlotEntity extends BaseEntity {

	@Id @Type(type = "pg-uuid") 
	private UUID id;

	@Builder.Default
	private LocalDateTime createdDate = LocalDateTime.now();
	
	@Builder.Default
	private LocalDateTime modifiedDate = LocalDateTime.now();
	
	private String createdBy;

	private String modifiedBy;
	
	@Version
	@Builder.Default
    private Long version = 1L;

	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private SlotRs resource;
	
	
//	public static class SlotEntityBuilder {
//		private SlotRs resource;
//		
//		// custom setter for resource: primary key and resource.id should be the same
//		public SlotEntityBuilder resource(SlotRs resource) {
//			if (this.id == null) { // in case of updating, no need to set the id
//				this.id(resource.getId());
//			}
//			this.resource = resource;
//			return this;
//		}
//	}
}
