package com.fhir.example.domain.resource;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CodingRs implements Serializable {
	private String system;
	private String version;
	private String code;
	private String display;
	private Boolean userSelected;
}
