package com.fhir.example.domain;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Embeddable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder
public class HistoryEntityId implements Serializable {
	
	private UUID id;
	private Long version;
}
