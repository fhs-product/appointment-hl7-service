package com.fhir.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.fhir.example.domain.AppointmentHistoryEntity;
import com.fhir.example.domain.HistoryEntityId;

public interface AppointmentHistoryRepository extends CrudRepository<AppointmentHistoryEntity, HistoryEntityId> {

	// select * from appointment_history where resource @> '{"id" : "b93fdb6d-652a-4b04-a49a-75c7dfc652cb", "meta" : {"versionId" : 1} }';
}
