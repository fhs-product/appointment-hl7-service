package com.fhir.example.domain.resource.validation;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

public class ResourceValidator {

	private final BooleanSupplier test;     // lambda taking no parameters and returning a Boolean
	private final Supplier<String> message; // lambda taking no parameters and returning a String

	/**
	 *
	 * @param test lambda function to evaluate
	 * @param message lambda function returning text message in exception
	 */
	public ResourceValidator(final BooleanSupplier test, final Supplier<String> message) {
		this.test = test;
		this.message = message;
	}

	/**
	 * performs the validation check
	 * executes the test and throws and  throws an InvalidResource exception
	 * using the derived message
	 */
	public void execute() {
		if (test.getAsBoolean()) {
			throw new IllegalArgumentException(message.get());
		}
	}

	/**
	 * static method to perform validation checks
	 * @param validators Array of ValidationCheck objects
	 */
	public static void execute(final ResourceValidator[] validators) {
		for (ResourceValidator vc : validators) {
			vc.execute();
		}
	}
}
