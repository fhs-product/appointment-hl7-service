package com.fhir.example.domain.resource;

import java.io.Serializable;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CodeableConceptRs implements Serializable {

	private List<CodingRs> coding;
	private String text;
}
