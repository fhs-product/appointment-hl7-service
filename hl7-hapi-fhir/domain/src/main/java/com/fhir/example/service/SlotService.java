package com.fhir.example.service;

import java.util.UUID;

import com.fhir.example.domain.resource.SlotRs;

public interface SlotService {
	SlotRs create(SlotRs slot);

	SlotRs findById(UUID id);
}
