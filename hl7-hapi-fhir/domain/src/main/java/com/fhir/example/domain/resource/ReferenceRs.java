package com.fhir.example.domain.resource;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ReferenceRs implements Serializable {
	private String reference;
	private String type;
	private IdentifierRs identifier;
	private String display;
}
