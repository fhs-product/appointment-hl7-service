package com.fhir.example.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fhir.example.domain.resource.AppointmentRs;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Entity
@Table(name = "appointment_history")
public class AppointmentHistoryEntity extends BaseEntity {
	
	@EmbeddedId
    private HistoryEntityId id;

	@Builder.Default
	private LocalDateTime createdDate = LocalDateTime.now();

	@Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
	private AppointmentRs resource;
	
}
