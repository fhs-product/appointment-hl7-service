package com.fhir.example.domain.resource;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class IdentifierRs implements Serializable {

	private String use;
	private CodeableConceptRs type;
	private String system;
	private String value;
	private PeriodRs period;
}
