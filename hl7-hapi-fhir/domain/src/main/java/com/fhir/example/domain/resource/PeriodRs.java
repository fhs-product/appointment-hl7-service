package com.fhir.example.domain.resource;

import java.io.Serializable;
import java.time.LocalDate;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PeriodRs implements Serializable {

	private LocalDate start;
	private LocalDate end;
}
