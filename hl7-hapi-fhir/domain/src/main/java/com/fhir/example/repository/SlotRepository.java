package com.fhir.example.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.fhir.example.domain.SlotEntity;

public interface SlotRepository extends CrudRepository<SlotEntity, UUID> {

}
