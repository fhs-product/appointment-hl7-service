package com.fhir.example.domain.resource;

import java.io.Serializable;
import java.util.UUID;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder(toBuilder = true)
public class Resource implements Serializable {

	@Builder.Default
	protected UUID id = UUID.randomUUID();
	protected String resourceType;
	@Builder.Default
	protected MetaRs meta = MetaRs.builder().build();
	protected String implicitRules;
	@Builder.Default
	protected String language = "en";
}
