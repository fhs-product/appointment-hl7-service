package com.fhir.example.service;

import java.util.List;
import java.util.UUID;

import com.fhir.example.domain.resource.AppointmentRs;

public interface AppointmentService {
	AppointmentRs create(AppointmentRs appointment);

	AppointmentRs update(UUID id, AppointmentRs appointment);

	AppointmentRs findById(UUID id);

	List<AppointmentRs> findAll();

	List<AppointmentRs> findByActorDisplayName(String actorDisplayName);

	AppointmentRs findByHistory(UUID id, Long versionId);
}
