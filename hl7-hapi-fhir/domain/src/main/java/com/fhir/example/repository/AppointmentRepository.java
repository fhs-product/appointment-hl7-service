package com.fhir.example.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fhir.example.domain.AppointmentEntity;
import com.fhir.example.domain.resource.AppointmentRs;

@Repository
public interface AppointmentRepository extends JpaRepository<AppointmentEntity, UUID> {

	@Transactional
	@Modifying
	@Query(value = "UPDATE AppointmentEntity SET modifiedDate = now(), resource = :resource, version = version + 1 WHERE id = :id and version = :currentVersion")
	int updateResource(@Param("id") UUID id, @Param("currentVersion") Long currentVersion, @Param("resource") AppointmentRs resource);

	//	@Query(value = "Select * from appointment where jsonb_path_exists(resource, '$.participant[*].actor.display ? (@ like_regex :displayName)')", nativeQuery = true)
	//	List<AppointmentEntity> findByActorDisplayName(@Param("displayName") String displayName);

	@Query(value = "Select * from appointment where jsonb_path_exists(resource, '$.participant[*].actor.display ? ( @ like_regex \"James\" )' )", nativeQuery = true)
	List<AppointmentEntity> findByActorDisplayName(String display);
}
