CREATE TABLE slot
(
   id               UUID NOT NULL,
   created_date     TIMESTAMP (6),
   modified_date    TIMESTAMP (6),
   created_by       VARCHAR(255),
   modified_by      VARCHAR(255),
   version          BIGINT,
   resource         JSONB,
   PRIMARY KEY (id)
);

CREATE TABLE appointment
(
   id               UUID NOT NULL,
   created_date     TIMESTAMP (6),
   modified_date    TIMESTAMP (6),
   created_by       VARCHAR(255),
   modified_by      VARCHAR(255),
   version          BIGINT,
   resource         JSONB,
   PRIMARY KEY (id)
);

CREATE TABLE appointment_history
(
   id               UUID NOT NULL,
   version          BIGINT,
   created_date     TIMESTAMP (6),
   resource         JSONB,
   PRIMARY KEY (id, version)
);