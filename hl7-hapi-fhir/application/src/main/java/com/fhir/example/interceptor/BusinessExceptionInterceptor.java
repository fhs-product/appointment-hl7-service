package com.fhir.example.interceptor;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.NoSuchElementException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.OperationOutcome.IssueSeverity;
import org.hl7.fhir.r4.model.OperationOutcome.IssueType;
import org.hl7.fhir.r4.model.OperationOutcome.OperationOutcomeIssueComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Interceptor;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.exceptions.BaseServerResponseException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import ca.uhn.fhir.rest.server.servlet.ServletRequestDetails;

@Interceptor
public class BusinessExceptionInterceptor {

	private final Logger LOGGER = LoggerFactory.getLogger(BusinessExceptionInterceptor.class);

	@Hook(Pointcut.SERVER_PRE_PROCESS_OUTGOING_EXCEPTION)
	public BaseServerResponseException handleException(final RequestDetails theRequestDetails,
			final ServletRequestDetails servletRequestDetails, final Throwable throwable, final HttpServletRequest theServletRequest,
			final HttpServletResponse theServletResponse) throws ServletException, IOException {

		Throwable t = throwable.getCause();

		if (t instanceof InvocationTargetException) {
			if (t.getCause() instanceof NoSuchElementException) {
				OperationOutcomeIssueComponent ooIssue = new OperationOutcomeIssueComponent();
				ooIssue.setSeverity(IssueSeverity.ERROR).setCode(IssueType.NOTFOUND)
				.setDiagnostics(t.getCause().getMessage());
				OperationOutcome operationOutcome = new OperationOutcome();
				operationOutcome.addIssue(ooIssue);

				ResourceNotFoundException a = new ResourceNotFoundException(t.getCause().getMessage());
				a.setOperationOutcome(operationOutcome);
				return a;
			}
			if (t.getCause() instanceof IllegalArgumentException) {
				OperationOutcomeIssueComponent ooIssue = new OperationOutcomeIssueComponent();
				ooIssue.setSeverity(IssueSeverity.ERROR).setCode(IssueType.INVALID)
				.setDiagnostics(t.getCause().getMessage());
				OperationOutcome operationOutcome = new OperationOutcome();
				operationOutcome.addIssue(ooIssue);

				UnprocessableEntityException a = new UnprocessableEntityException(t.getCause().getMessage());
				a.setOperationOutcome(operationOutcome);
				return a;
			}
		}

		return null;
	}

}