package com.fhir.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;

@ServletComponentScan
@SpringBootApplication
@ComponentScan(basePackages = "com.fhir.example.*")
public class FhirDemoApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(FhirDemoApplication.class);

	public static void main(final String[] args) {
		SpringApplication.run(FhirDemoApplication.class, args);
		LOGGER.info("FhirDemoApplication Started........");
	}
}
