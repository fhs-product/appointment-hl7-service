package com.fhir.demo.business;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserBO {
	private String username;
	private String firstName;
	private String lastName;
	private String password;

}
