package com.fhir.demo.config;

import org.keycloak.admin.client.Keycloak;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class KeycloakConfig {
	public static Keycloak INSTANCE;

	private String authServerUrl;
	private String masterRealm;
	private String realm;
	private String clientId;
	private String userName;
	private String password;
}
