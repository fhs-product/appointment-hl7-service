package com.fhir.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserServiceConfiguration {

	@Bean
	@ConfigurationProperties(prefix = "keycloak")
	public KeycloakConfig keycloakConfig() {
		return new KeycloakConfig();
	}

}
