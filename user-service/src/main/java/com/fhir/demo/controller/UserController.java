package com.fhir.demo.controller;

import java.util.List;

import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fhir.demo.business.UserBO;
import com.fhir.demo.dto.UserDTO;
import com.fhir.demo.mapper.UserMapper;
import com.fhir.demo.service.KeycloakAdminClientService;

@RestController
@RequestMapping("users")
public class UserController {

	@Autowired
	KeycloakAdminClientService keycloakAdminClientService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public void createUser(@RequestBody final UserDTO userDTO) {
		UserBO userBO = UserMapper.INSTANCE.toBusinessObject(userDTO);
		keycloakAdminClientService.addUser(userBO);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<UserRepresentation> getAllUsers() {
		return keycloakAdminClientService.getAllUsers();
	}

}