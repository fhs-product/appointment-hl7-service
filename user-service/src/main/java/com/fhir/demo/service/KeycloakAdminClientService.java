package com.fhir.demo.service;

import java.util.List;

import org.keycloak.representations.idm.UserRepresentation;

import com.fhir.demo.business.UserBO;

public interface KeycloakAdminClientService {

	void addUser(UserBO user);

	List<UserRepresentation> getAllUsers();
}
