package com.fhir.demo.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fhir.demo.business.UserBO;
import com.fhir.demo.config.KeycloakConfig;
import com.fhir.demo.service.KeycloakAdminClientService;

@Service("keycloakAdminClientService")
public class KeycloakAdminClientServiceImpl implements KeycloakAdminClientService {

	@Autowired
	private KeycloakConfig KeycloakConfig;

	@Override
	public void addUser(final UserBO user) {
		Keycloak keycloak = KeycloakBuilder.builder()
				.serverUrl(KeycloakConfig.getAuthServerUrl())
				.realm(KeycloakConfig.getMasterRealm())
				.grantType(OAuth2Constants.PASSWORD)
				.username(KeycloakConfig.getUserName())
				.password(KeycloakConfig.getPassword())
				.clientId(KeycloakConfig.getClientId())
				.resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
				.build();

		// Get realm
		RealmResource realmResource = keycloak.realm(KeycloakConfig.getRealm());
		UsersResource usersResource = realmResource.users();

		// Define user
		UserRepresentation kcUser = new UserRepresentation();
		kcUser.setUsername(user.getUsername());
		kcUser.setCredentials(Collections.singletonList(createPasswordCredentials(user.getPassword())));
		kcUser.setFirstName(user.getFirstName());
		kcUser.setLastName(user.getLastName());
		kcUser.setEnabled(true);

		// Create user
		Response response = usersResource.create(kcUser);
		System.out.printf("Repsonse: %s %s%n", response.getStatus(), response.getStatusInfo());
		System.out.println(response.getLocation());
		String userId = CreatedResponseUtil.getCreatedId(response);
		usersResource.create(kcUser);

		System.out.printf("User created with userId: %s%n", userId);

		// assign user role
		RoleRepresentation userRole = realmResource.roles().get("user").toRepresentation();
		usersResource.get(userId).roles().realmLevel().add(Arrays.asList(userRole));
	}

	@Override
	public List<UserRepresentation> getAllUsers() {
		Keycloak keycloak = KeycloakBuilder.builder()
				.serverUrl(KeycloakConfig.getAuthServerUrl())
				.realm(KeycloakConfig.getMasterRealm())
				.grantType(OAuth2Constants.PASSWORD)
				.username(KeycloakConfig.getUserName())
				.password(KeycloakConfig.getPassword())
				.clientId(KeycloakConfig.getClientId())
				.resteasyClient(new ResteasyClientBuilder().connectionPoolSize(10).build())
				.build();
		return keycloak.realm(KeycloakConfig.getRealm()).users().list();
	}

	private static CredentialRepresentation createPasswordCredentials(final String password) {
		CredentialRepresentation passwordCredentials = new CredentialRepresentation();
		passwordCredentials.setTemporary(false);
		passwordCredentials.setType(CredentialRepresentation.PASSWORD);
		passwordCredentials.setValue(password);
		return passwordCredentials;
	}
}
