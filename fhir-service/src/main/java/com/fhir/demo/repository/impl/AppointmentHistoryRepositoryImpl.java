package com.fhir.demo.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.fhir.demo.model.AppointmentHistory;
import com.fhir.demo.repository.AppointmentHistoryRepository;

@Repository
public class AppointmentHistoryRepositoryImpl implements AppointmentHistoryRepository {

	private final MongoTemplate mongoTemplate;

	@Autowired
	public AppointmentHistoryRepositoryImpl(final MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public AppointmentHistory save(final AppointmentHistory appointmentHistory) {
		return mongoTemplate.save(appointmentHistory);
	}

	@Override
	public List<AppointmentHistory> findAll() {
		return mongoTemplate.findAll(AppointmentHistory.class);
	}

	@Override
	public AppointmentHistory findByHistory(final String id, final Long versionId) {
		Query query = new Query();
		query.addCriteria(
				new Criteria().andOperator(
						Criteria.where("meta.versionId").is(versionId),
						Criteria.where("appointmentId").is(id)
						)
				);

		return mongoTemplate.findOne(query, AppointmentHistory.class);
	}

}
