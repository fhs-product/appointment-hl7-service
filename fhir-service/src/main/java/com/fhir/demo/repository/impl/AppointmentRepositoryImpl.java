package com.fhir.demo.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

import com.fhir.demo.model.Appointment;
import com.fhir.demo.repository.AppointmentRepository;

@Repository
public class AppointmentRepositoryImpl implements AppointmentRepository {

	private final MongoTemplate mongoTemplate;

	@Autowired
	public AppointmentRepositoryImpl(final MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public Appointment save(final Appointment appointment) {
		return mongoTemplate.save(appointment);
	}

	@Override
	public Appointment findById(final String id) {
		return mongoTemplate.findById(id, Appointment.class);
	}

	@Override
	public List<Appointment> findAll() {
		return mongoTemplate.findAll(Appointment.class);
	}

}
