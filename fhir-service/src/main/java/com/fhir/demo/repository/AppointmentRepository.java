package com.fhir.demo.repository;

import java.util.List;

import com.fhir.demo.model.Appointment;

public interface AppointmentRepository {
	Appointment save(Appointment appointment);

	Appointment findById(String id);

	List<Appointment> findAll();
}
