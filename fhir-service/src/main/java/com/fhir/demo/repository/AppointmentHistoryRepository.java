package com.fhir.demo.repository;

import java.util.List;

import com.fhir.demo.model.AppointmentHistory;

public interface AppointmentHistoryRepository {
	AppointmentHistory save(AppointmentHistory appointmentHistory);

	List<AppointmentHistory> findAll();

	AppointmentHistory findByHistory(String id, Long  versionId);
}
