package com.fhir.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fhir.demo.dto.AppointmentDto;
import com.fhir.demo.mapper.AppointmentMapper;
import com.fhir.demo.model.Appointment;
import com.fhir.demo.model.AppointmentHistory;
import com.fhir.demo.repository.AppointmentHistoryRepository;
import com.hir.demo.service.AppointmentService;

@RestController
@RequestMapping("Appointment")
public class AppointmentController {

	private AppointmentMapper mapper = AppointmentMapper.INSTANCE;

	@Autowired
	private AppointmentService appointmentService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public AppointmentDto createAppointment(@RequestBody final AppointmentDto appointmentDto) {
		Appointment appointment = mapper.toModel(appointmentDto);

		appointment = appointmentService.create(appointment);

		return mapper.toDto(appointment);
	}

	@GetMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AppointmentDto findOne(@PathVariable final String id) {

		Appointment appointment = appointmentService.findById(id);

		return mapper.toDto(appointment);
	}

	@GetMapping(value="/{id}/_history/{versionId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AppointmentDto history(@PathVariable final String id, @PathVariable final Long versionId) {

		Appointment appointment = appointmentService.findByHistory(id, versionId);

		return mapper.toDto(appointment);
	}

	@PutMapping(value="/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AppointmentDto update(@PathVariable final String id, @RequestBody final AppointmentDto appointmentDto) {

		Appointment appointment = appointmentService.update(mapper.toModel(appointmentDto));

		return mapper.toDto(appointment);
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentDto> findAll() {
		List<Appointment> appointments = appointmentService.findAll();

		return mapper.toDtos(appointments);
	}



	@Autowired
	private AppointmentHistoryRepository appointmentHistoryRepository;
	@GetMapping(value="/test", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AppointmentHistory> test() {

		return appointmentHistoryRepository.findAll();
	}
}
