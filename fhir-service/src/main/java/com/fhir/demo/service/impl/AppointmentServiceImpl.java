package com.fhir.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fhir.demo.mapper.AppointmentMapper;
import com.fhir.demo.model.Appointment;
import com.fhir.demo.model.AppointmentHistory;
import com.fhir.demo.model.Meta;
import com.fhir.demo.repository.AppointmentHistoryRepository;
import com.fhir.demo.repository.AppointmentRepository;
import com.hir.demo.service.AppointmentService;

@Service("appointmentService")
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Autowired
	private AppointmentHistoryRepository appointmentHistoryRepository;

	@Override
	public Appointment create(final Appointment appointment) {
		Meta meta = appointment.getMeta();
		if (meta == null) {
			meta = new Meta();

		}
		meta.setVersionId(1L);
		appointment.setMeta(meta);

		return appointmentRepository.save(appointment);
	}

	@Override
	public Appointment findById(final String id) {
		return appointmentRepository.findById(id);
	}

	@Override
	public List<Appointment> findAll() {
		return appointmentRepository.findAll();
	}

	@Transactional
	@Override
	public Appointment update(final Appointment appointment) {
		Appointment existingAppointment = findById(appointment.getId());
		if (existingAppointment == null) {
			create(appointment);
		}

		AppointmentHistory appointmentHistory = AppointmentMapper.INSTANCE.toHistory(existingAppointment);

		appointmentHistoryRepository.save(appointmentHistory);

		long newVersionId = existingAppointment.getMeta().getVersionId() + 1;
		appointment.getMeta().setVersionId(newVersionId);


		return appointmentRepository.save(appointment);
	}

	@Override
	public Appointment findByHistory(final String id, final Long versionId) {

		AppointmentHistory appointmentHistory = appointmentHistoryRepository.findByHistory(id, versionId);

		return AppointmentMapper.INSTANCE.toModel(appointmentHistory);
	}

}
