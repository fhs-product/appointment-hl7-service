package com.fhir.demo.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class ParticipantDto {
	private List<CodeableConceptDto> type;
	private ReferenceDto actor;
	private String required;
	private String status;
	private PeriodDto period;
}
