package com.fhir.demo.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fhir.demo.model.Coding;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
@JsonInclude(Include.NON_NULL)
public class MetaDto {

	private Long versionId;
	private LocalDateTime lastUpdated;;
	private String source;
	private List<Coding> security;
	private List<Coding> tag;
}
