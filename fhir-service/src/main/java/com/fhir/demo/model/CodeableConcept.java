package com.fhir.demo.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CodeableConcept {

	private List<Coding> coding;
	private String text;
}
