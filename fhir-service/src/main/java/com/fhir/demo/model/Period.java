package com.fhir.demo.model;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Period {

	private LocalDate start;
	private LocalDate end;
}
