package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Reference {
	private String reference;
	private String type;
	private Identifier identifier;
	private String display;
}
