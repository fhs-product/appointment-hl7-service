package com.fhir.demo.model;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@Document(collection = "appointment")
public class Appointment extends DomainResource {
	@Id
	private String id;
	private List<Identifier> identifier;
	private String status;
	private CodeableConcept cancelationReason;
	private List<CodeableConcept> serviceCategory;
	private List<CodeableConcept> serviceType;
	private List<CodeableConcept> specialty;
	private CodeableConcept appointmentType;
	private List<CodeableConcept> reasonCode;
	private int priority;
	private String description;
	private int minutesDuration;
	private List<Reference> slot;
	//	@JsonFormat(pattern = "dd/MM/yyyy hh:mm:ss a")
	//	private LocalDateTime created;
	private LocalDate created;
	private String comment;
	private List<Participant> participant;
	private List<Period> requestedPeriod;
}
