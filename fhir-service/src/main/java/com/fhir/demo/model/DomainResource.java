package com.fhir.demo.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public abstract class DomainResource extends Resource {

	protected Narrative text;
	// ...
}
