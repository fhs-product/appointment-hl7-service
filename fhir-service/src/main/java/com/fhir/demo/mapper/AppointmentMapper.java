package com.fhir.demo.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.fhir.demo.dto.AppointmentDto;
import com.fhir.demo.model.Appointment;
//import com.fhir.demo.model.AppointmentHistory;
import com.fhir.demo.model.AppointmentHistory;

@Mapper
public interface AppointmentMapper {
	AppointmentMapper INSTANCE = Mappers.getMapper(AppointmentMapper.class);

	Appointment toModel(AppointmentDto appointmentDto);
	AppointmentDto toDto(Appointment appointment);

	List<AppointmentDto> toDtos(List<Appointment> appointment);

	@Mapping(source = "id", target = "appointmentId")
	AppointmentHistory toHistory(Appointment appointment);

	@Mapping(source = "appointmentId", target = "id")
	Appointment toModel(AppointmentHistory appointmentHistory);
}


