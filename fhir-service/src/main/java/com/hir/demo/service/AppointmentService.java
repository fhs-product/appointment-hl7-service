package com.hir.demo.service;

import java.util.List;

import com.fhir.demo.model.Appointment;

public interface AppointmentService {
	Appointment create(Appointment appointment);

	Appointment update(Appointment appointment);

	Appointment findById(String id);

	List<Appointment> findAll();

	Appointment findByHistory(String id, Long versionId);
}
